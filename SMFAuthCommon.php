<?php

/**
* SMFAuth - Simple Machines Forum (SMF) Authentication Module
* Copyright (C) 2014 Moein Alinaghian <nixoeen at nixoeen dot com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 *
 * SMF Authentication Common Functions - SMFAuthCommon.php
 *
 * Functions to get the user's information.
 *
 * Based on the codes of:
 *   Josh Hartman - warpconduit.net
 *
 */

class SMFAuthCommon
{
	function getLocalUserInfo( $username, $password, $hashed=false )
	{
		global $wgSMFDB,$wgSMFAllowSpace;

		// Replace the spaces in Username with underscore
		if ($wgSMFAllowSpace == false)
			$username = str_replace(" ", "_", $username);

		$conn = new mysqli($wgSMFDB["host"], $wgSMFDB["user"], $wgSMFDB["pass"], $wgSMFDB["db"]);

		if ($conn->connect_error)
			die("Database connection failed: " . $conn->connect_error);

		$conn->set_charset("utf8");

		if (!$hashed)
			$password = sha1(strtolower($username).$password);
		$query = $conn->prepare("SELECT * FROM `".$wgSMFDB["prefix"]."members` WHERE `member_name` = ? AND `passwd` = ? LIMIT 1;");
		$query->bind_param("ss", $username, $password);
		$query->execute();

		$result = $this->getResult($query);

		$query->close();
		$conn->close();

		// Do not pass sensetive information
		unset($result["passwd"]);
		unset($result["password_salt"]);
		unset($result["secret_question"]);
		unset($result["secret_answer"]);

		return $result;
	}

	function getRemoteUserInfo( $username, $password, $hashed=false )
	{
		global $wgSMFRemoteURL,$wgSMFAllowSpace;

		// Replace the spaces in Username with underscore
		if ($wgSMFAllowSpace == false)
			$username = str_replace(" ", "_", $username);

		$params["current_time"] = time();
		$params["protocol"] = "v1";
		$params["username"] = $username;
		if (!$hashed)
			$params["password"] = sha1(strtolower($username).$password);
		else
			$params["password"] = $password;

		$curl = curl_init($wgSMFRemoteURL);
		curl_setopt( $curl, CURLOPT_POST, 1);
		curl_setopt( $curl, CURLOPT_POSTFIELDS, $this->encrypt($params));
		curl_setopt( $curl, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt( $curl, CURLOPT_HEADER, 0);
		curl_setopt( $curl, CURLOPT_RETURNTRANSFER, 1);

		$user = $this->decrypt(curl_exec($curl));

		if (!is_array($user))
			return "";

		if ($user["current_time"] == $params["current_time"])
			return $user;
		else
			return "";	
	}

	function sendRemoteUserInfo( $encrypted_input )
	{
		$login = $this->decrypt($encrypted_input);

		if (!is_array($login))
			return "";

		if ($login["protocol"] != "v1")
			return "";

		$user = $this->getLocalUserInfo($login["username"], $login["password"], true);
		if (!is_array($user))
			return "";

		$user["current_time"] = $login["current_time"];

		return $this->encrypt($user);
	}

	function encrypt($encrypt)
	{
		global $wgSMFRemoteKey;

		$key = pack('H*', $wgSMFRemoteKey);
		$encrypt = serialize($encrypt);
		$iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC), MCRYPT_DEV_URANDOM);
		$mac = hash_hmac('sha256', $encrypt, substr(bin2hex($key), -32));
		$passcrypt = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $encrypt.$mac, MCRYPT_MODE_CBC, $iv);
		$encoded = base64_encode($passcrypt).'|'.base64_encode($iv);

		return $encoded;
	}

	function decrypt($decrypt)
	{
		global $wgSMFRemoteKey;

		$key = pack('H*', $wgSMFRemoteKey);
		$decrypt = explode('|', $decrypt.'|');
		$decoded = base64_decode($decrypt[0]);
		$iv = base64_decode($decrypt[1]);
		if (strlen($iv) !== mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC))
			return "";
		$decrypted = trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, $decoded, MCRYPT_MODE_CBC, $iv));
		$mac = substr($decrypted, -64);
		$decrypted = substr($decrypted, 0, -64);
		$calcmac = hash_hmac('sha256', $decrypted, substr(bin2hex($key), -32));
		if($calcmac !== $mac)
			return "";
		$decrypted = unserialize($decrypted);

		return $decrypted;
	}

	function getResult($stmt)
	{
		// In case the MySQL Native Driver is installed
		if (function_exists("mysqli_stmt_get_result"))
			return $stmt->get_result()->fetch_array(MYSQLI_ASSOC);
		else
		{
			$md = $stmt->result_metadata();
			$params = array();
			while ($field = $md->fetch_field())
				$params[] = &$result[$field->name];
			call_user_func_array(array($stmt, "bind_result"), $params);
			if($stmt->fetch())
				return $result;
			else
				return "";
		}
	}
}
?>
