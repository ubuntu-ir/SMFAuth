<?php

/**
* SMFAuth - Simple Machines Forum (SMF) Authentication Module
* Copyright (C) 2014 Moein Alinaghian <nixoeen at nixoeen dot com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

	require("config.php");

	if (!isset($wgSMFType))
		die();
	if ($wgSMFType != "remote-smf")
		die();

	require_once("SMFAuthCommon.php");
	$SMFAuthCommon = new SMFAuthCommon();

	echo $SMFAuthCommon->sendRemoteUserInfo(file_get_contents('php://input'));

?>