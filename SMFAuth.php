<?php

/**
* SMFAuth - Simple Machines Forum (SMF) Authentication Module
* Copyright (C) 2014 Moein Alinaghian <nixoeen at nixoeen dot com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 *
 * SMF Authentication - SMFAuth.php
 *
 * MediaWiki password authentication using the database of SMF, developed
 * to be used in ubuntu.ir.
 *
 * Based on the codes of:
 *   Ryan Lane <ryan at wikimedia dot org>
 *   Snakehit <snakehit at gmail dot com>
 *
 */

if ( !defined( 'MEDIAWIKI' ) ) exit;

// Load common function to get the user information
require_once("SMFAuthCommon.php");
$wgSMF = new SMFAuthCommon();

// Disable creating new users
$wgGroupPermissions['*']['createaccount'] = false;

class SMFAuthPlugin extends AuthPlugin
{
	function userExists( $username )
	{
		// We do not want to add user to SMF, so we don't need this function
		return true;
	}

	function authenticate( $username, $password )
	{
		global $wgSMF, $wgSMFType;

		if ($wgSMFType == "remote-mediawiki")
			$result = $wgSMF->getRemoteUserInfo($username, $password);
		else
			$result = $wgSMF->getLocalUserInfo($username, $password);
		
		if (!is_array($result))
			return false;

		$this->user["username"] = $result["member_name"];
		$this->user["group"] = $result["id_group"];
		$this->user["name"] = $result["real_name"];
		$this->user["email"] = $result["email_address"];

		return true;
	}

	function modifyUITemplate( &$template, &$type )
	{
		// Disable remember feature
		$template->set('canremember', false);
	}

	function validDomain( $domain )
	{
		return true;
	}

	function updateUser( &$user )
	{
		global $wgSMFGroup;

		$user->setEmail($this->user["email"]);
		$user->setRealName($this->user["name"]);
		$user->confirmEmail();

		$found = false;
		if (isset($wgSMFGroup[$this->user["group"]]))
			$current_group = "SMF_".$wgSMFGroup[$this->user["group"]];
		else
			$current_group = "smf_".$this->user["group"];

		foreach ($user->getEffectiveGroups() as $group)
			if (substr($group,0,4) == "smf_" || substr($group,0,4) == "SMF_")
				if ($group == $current_group)
				{
					$found = true;
					break;
				}
				else
					$user->removeGroup($group);
		if (!$found)
			$user->addGroup($current_group);

		$user->saveSettings();

		return true;
	}

	function autoCreate()
	{
		// Add users which available in SMF but not in local database
		return true;
	}

	function allowPasswordChange()
	{
		return false;
	}

	function allowSetLocalPassword()
	{
		return false;
	}

	function setPassword( $user, $password )
	{
		return false;
	}

	function updateExternalDB( $user )
	{
		// We do not change anything, but don't want to get any error as well
		return true;
	}

	function updateExternalDBGroups( $user, $addgroups, $delgroups = array() )
	{
		return true;
	}

	function canCreateAccounts()
	{
		return false;
	}

	function addUser( $user, $password, $email='', $realname='' ) {
		return false;
	}

	function strict()
	{
	 	// Return true to prevent logins that don't authenticate here from being
	 	// checked against the local database's password fields.
		return true;
	}

	function strictUserAuth( $username )
	{
		return true;
	}

	function initUser( &$user, $autocreate=false )
	{
		$user->mPassword = '';
		$this->updateUser($user);

		return true;
	}

	function getCanonicalName( $username )
	{
		if ($username != '')
			// The wiki considers an all lowercase name to be invalid; need to
			// uppercase the first letter
			$username = ucfirst($username);

		return $username;
	}
}

$wgExtensionCredits['other'][] =
	array(
		'name' => 'SMF Authentication Plugin',
		'version' => '2.0',
		'author' => 'Moein Alinaghian',
		'description' => 'SMF Authentication plugin',
		'url' => 'http://nixoeen.com',
		'license-name' => 'GPLv3',
	);

?>
